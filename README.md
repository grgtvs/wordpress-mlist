# MList
an absolutely free newsletter plugin!
------

##Features Notes

### Version 0.1

* Unlimited subscribers
* Automatic subscribe current users and new users
* Send unlimeted emails into batches (using wp_mail() only)
* Unlimited mailing lists
* Scheduled newsletters (Repeating or One-Off)
* Queue emails
* Managing Subscribers
* Managing Mailing list
* Managing Newsletter Templates
* Mail tracking
* Multisite support